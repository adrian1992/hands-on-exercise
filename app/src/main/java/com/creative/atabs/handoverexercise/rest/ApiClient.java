package com.creative.atabs.handoverexercise.rest;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by adriantabsy on 1/11/19.
 */

public interface ApiClient {

    @GET("news")
    Observable<Object> getUFCNews();

}
