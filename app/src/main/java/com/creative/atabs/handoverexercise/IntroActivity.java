package com.creative.atabs.handoverexercise;

import android.app.Activity;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.creative.atabs.handoverexercise.rest.ApiClient;
import com.creative.atabs.handoverexercise.rest.ServiceGenerator;
import com.creative.atabs.handoverexercise.utils.UrlsConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by adriantabsy on 1/11/19.
 */

public class IntroActivity extends Activity {

    @BindView(R.id.activity_main_get_data_btn)
    Button mGetDataBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mGetDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start implementation in getting the news feeds.
                ApiClient client = ServiceGenerator.createService(ApiClient.class,
                        UrlsConstant.NEWS_URL);

                client.getUFCNews().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Object>() {

                            /**
                             * Provides the Observer with the means of cancelling (disposing) the
                             * connection (channel) with the Observable in both
                             * synchronous (from within {@link #onNext(Object)}) and asynchronous manner.
                             *
                             * @param d the Disposable instance whose {@link Disposable#dispose()} can
                             *          be called anytime to cancel the connection
                             * @since 2.0
                             */
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            /**
                             * Provides the Observer with a new item to observe.
                             * <p>
                             * The {@link Observable} may call this method 0 or more times.
                             * <p>
                             * The {@code Observable} will not call this method again after it calls either {@link #onComplete} or
                             * {@link #onError}.
                             *
                             * @param value the item emitted by the Observable
                             */
                            @Override
                            public void onNext(Object value) {

                            }

                            /**
                             * Notifies the Observer that the {@link Observable} has experienced an error condition.
                             * <p>
                             * If the {@link Observable} calls this method, it will not thereafter call {@link #onNext} or
                             * {@link #onComplete}.
                             *
                             * @param e the exception encountered by the Observable
                             */
                            @Override
                            public void onError(Throwable e) {

                            }

                            /**
                             * Notifies the Observer that the {@link Observable} has finished sending push-based notifications.
                             * <p>
                             * The {@link Observable} will not call this method if it calls {@link #onError}.
                             */
                            @Override
                            public void onComplete() {

                            }
                        });
            }
        });
    }
}
