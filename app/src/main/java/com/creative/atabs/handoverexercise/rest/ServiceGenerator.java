package com.creative.atabs.handoverexercise.rest;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by adriantabsy on 1/10/19.
 */

public class ServiceGenerator {

    private static OkHttpClient.Builder httpClient;
    private static Retrofit.Builder builder;

    public static Retrofit.Builder getRetrofitInstance() {
        return builder;
    }

    public static <S> S createServiceRx(Class<S> serviceClass, String apiBaseURL) {

        httpClient = new OkHttpClient.Builder();
        builder = new Retrofit.Builder()
                .baseUrl(apiBaseURL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        return requestParams(serviceClass);
    }

    private static  <S> S requestParams(Class<S> serviceClass){


        OkHttpClient client = httpClient
                .connectTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = builder.client(client).build();

        //re-initialize for next calls

        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass, String apiBaseURL) {
        httpClient = new OkHttpClient.Builder();
        builder = new Retrofit.Builder()
                .baseUrl(apiBaseURL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        OkHttpClient client = httpClient
                .connectTimeout(3000, TimeUnit.SECONDS)
                .readTimeout(3000, TimeUnit.SECONDS)
                .writeTimeout(3000, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = builder.client(client).build();

        //re-initialize for next calls

        return retrofit.create(serviceClass);
    }



}
